; Takes a simple link and converts it to HTML
; © Drugwash, 2018.01.04-2023.08.17
;================================================================
;@Ahk2Exe-Obey bits, = %A_PtrSize% * 8
;@Ahk2Exe-Obey xbit, = "%A_PtrSize%" > 4 ? "x64" : "x86"
;@Ahk2Exe-Obey type, = "%A_IsUnicode%" ? "Unicode" : "ANSI"
;@Ahk2Exe-Obey xtype, = "%A_IsUnicode%" ? "W" : "A"
;@Ahk2Exe-Let version = 2.2.0.1
;@Ahk2Exe-AddResource %A_ScriptDir%\res\down80x80-32.bmp, 100
;@Ahk2Exe-AddResource %A_ScriptDir%\res\noimg80x80-32.bmp, 101
;@Ahk2Exe-AddResource *14 %A_ScriptDir%\res\FVL8.ico, 159
;@Ahk2Exe-SetFileVersion %U_version%
;@Ahk2Exe-SetProductVersion 2.2.0.0
;@Ahk2Exe-SetProductName Fix Video Link
;@Ahk2Exe-SetInternalName FixVideoLink %U_version%.ahk
;@Ahk2Exe-SetOrigFilename Fix Video Link.exe
;@Ahk2Exe-SetDescription FVL Formats simple URLs as HTML links with optional image
;@Ahk2Exe-SetCompanyName Drugwash Hobby Programming
;@Ahk2Exe-SetCopyright Copyright © Drugwash`, Jan 2018-Aug 2023
;@Ahk2Exe-SetLegalTrademarks Released under the terms of the GNU General Public License
;@Ahk2Exe-Set Comments, Compiled in %A_AhkVersion% %U_type% %U_bits%bit.
;@Ahk2Exe-SetMainIcon %A_ScriptDir%\res\FVL8.ico
;@Ahk2Exe-UpdateManifest 0, Drugwash.HobbyProgramming.FixVideoLink, %U_version%
;================================================================
#NoEnv
#SingleInstance Force
#KeyHistory 0
ListLines Off
SetBatchLines, -1
SetKeyDelay, -1, -1
SetMouseDelay, -1
SetWinDelay, -1
SetControlDelay, -1
SendMode Input
SetWorkingDir %A_ScriptDir%
CoordMode, Mouse, Screen
DetectHiddenWindows, On
global debug := false
global AW := A_IsUnicode ? "W" : "A"
;================================================================
author=Drugwash
appname=Fix Video Link
version=2.2.0.1
releaseD=Aug 17, 2023
releaseT=(cURL version)
releaseE := A_IsUnicode ? "Unicode" : "ANSI"
iconlocal=%A_ScriptDir%\res\FVL8.ico
;================================================================
Menu, Tray, Tip, %appname% %version%
if !A_IsCompiled
	Menu, Tray, Icon, %iconlocal%
Menu, Tray, NoStandard
Menu, Tray, Add, Show/Hide, showhide
Menu, Tray, Add, AlwaysOnTop, aot
Menu, Tray, Add
Menu, Tray, Add, About, about
Menu, Tray, Add
Menu, Tray, Add, Reload, reload
Menu, Tray, Add, Exit, GuiClose
Menu, Tray, Default, Show/Hide
Menu, Tray, Click, 1

global fxUrl, abort
global dataReady := 0
global useXML := 1	; maybe later on switch it automatically based on OS version (XP=0, Vista+=1)
ComObjError(debug ? true : false)
color := getSC()
OnMessage(0x15, "setABC")	; WM_SYSCOLORCHANGE
if !pToken := Gdip_On(hLibGdip)
	{
	msgbox, Error loading GdiPlus library.`nSite image will not be retrieved.
;	return
	}
if !FileExist("libcurl.dll")
	{
	FileInstall, libcurl.dll, libcurl.dll
	Sleep, 3000
	if !FileExist("libcurl.dll")
		{
		msgbox, Error deploying cUrl library.`nMake sure current folder is writable.`nApplication will now exit.
		ExitApp
		}
	}
curl_global_init()
hnd := curl_easy_init()
cb := RegisterCallback("write_callback", "C F")
curl_easy_setopt(hnd, "CURLOPT_WRITEFUNCTION", cb)
OnExit, unload
linkPage=
(
<!DOCTYPE html>
<html status="whitelisted" style="overflow-y:hidden;">
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
a:link {background-color: transparent; text-decoration: none;}
a:visited {background-color: transparent; text-decoration: none;}
a:hover {color: red; background-color: transparent; text-decoration: underline;}
a:active {color: #00D0D0; background-color: transparent; text-decoration: underline;}
</style>
</head>
<body style="margin:0 auto;padding:0px;background-color:%color%;">
<div id="testlink"
	style="font-family:'Segoe UI',Tahoma,Arial,sans-serif;
		font-size:11px;margin:0 auto;padding:0px;position:absolute;"></div>
</body></html>
)
if !A_IsCompiled
	{
	down := "res\down80x80-32.png"
	noimg := "res\noimg80x80-32.png"
	}
else
	{
	hDown := LoadImage(A_ScriptName, 100, "B")
	hNoimg := LoadImage(A_ScriptName, 101, "B")
	if !(hDown OR hNoimg)
		msgbox, can't load bitmap resources from %A_ScriptName%
	}
sh := 1, aot := 0

Gui +OwnDialogs
Gui, Margin, 2, 2
Gui, Add, GroupBox, x4 y4 w564 h48, Input (unformatted video link)
Gui, Add, Text, x10 y25 w40 h18 +Right -0x200, Link:
Gui, Add, Edit, x52 y22 w400 h24 -Multi vinUrl grefresh,
Gui, Add, Button, x454 yp w24 h24 Disabled gclear0, X
Gui, Font, Bold,
Gui, Add, Button, x484 y22 w80 h24 gpaste, Paste
Gui, Font, Normal,
Gui, Add, GroupBox, x4 y56 w564 h184, Video information
Gui, Font, Bold,
Gui, Add, Button, x484 y74 w80 h24 Disabled gfetch, Fetch
Gui, Font, Normal,
Gui, Add, Button, x484 y98 w80 h24 Disabled, Abort
Gui, Add, Text, x10 y74 w40 h18 +Right -0x200, Server:
Gui, Add, Edit, x52 y74 w100 h18 -Multi +ReadOnly -E0x300 vhost,
Gui, Add, Text, x154 y74 w40 h18 +Right -0x200, Owner:
Gui, Add, Edit, x196 y74 w180 h18 -Multi +ReadOnly -E0x300 vowner,
Gui, Add, Text, x10 y98 w40 h18 +Right -0x200, Time:
Gui, Add, Edit, x52 y98 w70 h18 -Multi +ReadOnly -E0x300 vtime, ;88:88:88:88
Gui, Add, Text, x154 y98 w40 h18 +Right -0x200, Size:
Gui, Add, Edit, x196 y98 w65 h18 -Multi +ReadOnly -E0x300 vdims, ;8888x8888
Gui, Add, Text, x264 y98 w40 h18 +Right -0x200, Added:
Gui, Add, Edit, x306 y98 w120 h18 -Multi +ReadOnly -E0x300 vadded, ;8888-88-88
Gui, Add, Text, x10 y124 w40 h18 +Right -0x200, Title:
Gui, Add, Edit, x52 y122 w400 h30 +Multi -0x200000 vtitle gchange,
Gui, Add, Button, x454 y125 w24 h24 Disabled gclear1, X
Gui, Add, Text, x10 y156 w40 h46 +Right -0x200, Des -`ncrip -`ntion:
Gui, Add, Edit, x52 y154 w400 h50 +Multi -0x200000 vdesc gchange,
Gui, Add, Button, x454 y167 w24 h24 Disabled gclear2, X
Gui, Add, Text, x10 y206 w40 h30 +Right -0x200, Cap -`ntion:
Gui, Add, Edit, x52 y206 w400 h30 -Multi -0x200000 hwndhEdit0 vcapt gchange,
Gui, Add, Button, x454 y210 w24 h24 Disabled gclear3, X
Gui, Add, Picture, x484 y123 w80 h80 +0x20E hwndhImg vimg gpreview
	, % A_IsCompiled ? "HBITMAP:*" hNoimg : noimg
Gui, Add, Button, x484 y210 w80 h24 Disabled vbUpdate gupdate, Update
Gui, Add, Text, x10 y262 w40 h18 +Right -0x200, Test:
Gui, Add, ActiveX, x52 y262 w512 h18 vtest, Shell.Explorer
Gui, Add, GroupBox, x4 y244 w564 h224, Output (HTML formatted link)
Gui, Add, Text, x10 y285 w40 h18 +Right -0x200, Code:
Gui, Add, Edit, x52 y282 w430 h154 +Multi T4 -0x202000 +0x40 voutCode,
Gui, Add, Text, x10 y440 w40 h24 +Right -0x200, Y2Mate:
Gui, Add, Edit, x52 y438 w390 h24 +Multi T4 -0x202000 +0x40 voutY2mate,
Gui, Add, Button, x+2 yp w40 h24 Disabled vbY2M gcopyY2M, Copy

Gui, Add, Radio, x484 y282 w80 h18 Checked vlinkType gltsel, Use text
Gui, Add, Radio, x484 y300 w80 h18 gltsel, Use picture
Gui, Add, Checkbox, x484 y322 w80 h18 Check3 Checked-1 vbSec gupdate, Secure link
Gui, Add, Checkbox, x484 y340 w80 h18 vbBold gupdate, Bold text
Gui, Add, Checkbox, x484 y358 w80 h18 vbServer gupdate, Server
Gui, Add, Checkbox, x484 y376 w80 h18 vbOwner gupdate, Owner
Gui, Add, Checkbox, x484 y394 w80 h18 vbTime gupdate, Duration
Gui, Add, Radio, x484 y340 w80 h18 Hidden Checked vbSzO gsis, Original size
Gui, Add, Radio, x484 y358 w30 h18 Hidden vbSzW gsis, W
Gui, Add, Radio, x484 y376 w30 h18 Hidden vbSzH gsis, H
Gui, Add, Radio, x484 y394 w80 h18 Hidden vbSzF gsis, Forced size
Gui, Add, Edit, x514 y358 w50 h18 -Multi Hidden Disabled vimgWSel gsis,
Gui, Add, UpDown, yp h18 +0x80 Range10-32742 Hidden vis1 gsis, 10
Gui, Add, Edit, x514 y376 w50 h18 -Multi Hidden Disabled vimgHSel gsis,
Gui, Add, UpDown, yp h18 +0x80 Range10-32742 Hidden vis2 gsis, 10
Gui, Font, Bold,
Gui, Add, Button, x484 y412 w80 h24 Disabled vbCopy gcopy, Copy code
Gui, Font, Normal,
Gui, Add, Button, x484 y438 w80 h24 gGuiClose, Exit
cT0 := "caption text for Picture mode (only for WP blogs; optional)", pcT0 := &cT0
SendMessage, 0x1501, 1, %pcT0% ,, ahk_id %hEdit0%		; EM_SETCUEBANNER

test.silent := true, test.Navigate("about:blank")
While (test.readystate != 4 or test.busy)
	Sleep 10
test.document.write(linkPage)
; Generated using SmartGuiXP Creator mod 4.4.0.7
Gui, Show, AutoSize Center, %appname% %version%
OnMessage(0x44, "OnMsgBox")
testRef := test.document.getElementById("testlink")
hCursM := DllCall("LoadCursor", "Ptr", NULL, "Int", 32646, "Ptr")	; IDC_SIZEALL
Gui, 2:Margin, 0, 0
Gui, 2:Color, 0, 0
Gui, 2:-Caption +ToolWindow +Border +AlwaysOnTop
Gui, 2:Add, Picture, w800 h600 +0xE AltSubmit hwndhPreview vPreview gmoveit,
Gui, 2:Show, AutoSize Center Hide, FVL Preview
hGui2 := WinExist("FVL Preview")
return
;================================================================
unload:
curl_easy_cleanup(hnd)
curl_global_cleanup()
Gdip_Off(hLibGdip, pToken)
DllCall("DeleteObject", "Ptr", hBitmap)
DllCall("DeleteObject", "Ptr", hBmp2)
if hDown
	DllCall("DeleteObject", "Ptr", hDown)
if hNoimg
	DllCall("DeleteObject", "Ptr", hNoimg)
OnMessage(0x44, "")

GuiClose:
ExitApp
;================================================================
moveit:
DllCall("SetCursor", "Ptr", hCursM)
PostMessage, 0xA1, 2,,, A	; WM_NCLBUTTONDOWN
return

preview:
Gui, 2:Show
return

2GuiContextMenu:
Gui, 2:Hide
return
;================================================================
paste:
GuiControl, Disable, bUpdate		; Update
; Stupid WINE allows \n \r \t in a single-line Edit control !!!
inUrl := RegExReplace(Clipboard, "m`a)\R")
GuiControl,, inUrl, %inUrl%
refresh:
Gui, Submit, NoHide
GuiControl, % (inUrl ? "Enable" : "Disable"), Button2	; Fetch
GuiControl, % (inUrl ? "Enable" : "Disable"), Button5	; Fetch
RegExMatch(inUrl, "(?<=)https?", oSec)
return

fetch:
abort := false
SetTimer, grab, -1
return

grab:
GuiControl, Disable, Button3	; Paste
GuiControl, Disable, Button5	; Fetch
GuiControl, Enable, Button6		; Abort
GuiControl, Disable, bUpdate	; Update
GuiControl, Disable, bCopy		; Copy code
GuiControl, Disable, bY2M		; Copy
GuiControl,, host,
GuiControl,, owner,
GuiControl,, time,
GuiControl,, dims,
GuiControl,, added,
GuiControl,, title,
GuiControl,, desc,
GuiControl,, capt,
if hBitmap
	DllCall("DeleteObject", "Ptr", hBitmap)
GuiControl,, img, % A_IsCompiled ? "HBITMAP:*" hDown : down
GuiControl,, outCode,
GuiControl,, outY2Mate,
testRef.innerHTML := ""
fxUrl := fixVideoLink(inUrl, bSec)
SetTimer, get, -1
return

get:
if debug
	{
	FileDelete, tmpImgFile*
	FileDelete, currentPage.txt
	FileDelete, debug.txt
	FileDelete, headers.txt
	}
getData(WR, fxUrl)
Loop, 10
	if !dataReady
		Sleep, 1500
	else break
dataReady := 0
gosub update
if image
	{
	if !hBitmap := Gdip_FitImageEx(image, hImg, 0)
		Loop, 10
			if !dataReady
				Sleep, 2500
			else break
	if (!hBitmap && !dataReady)
		GuiControl,, img, % A_IsCompiled ? "HBITMAP:*" hNoimg : noimg
	dataReady := 0
	}
else GuiControl,, img, % A_IsCompiled ? "HBITMAP:*" hNoimg : noimg
GuiControl, Enable, Button3		; Paste
GuiControl, Enable, Button5		; Fetch
GuiControl, Disable, Button6	; Abort
GuiControl, Enable, bUpdate		; Update
GuiControl, Enable, bCopy		; Copy code
GuiControl, Enable, bY2M		; Copy
;return

change:
Gui, Submit, NoHide
if inUrl
	GuiControl, Enable, bUpdate		; Update
GuiControl, % (title ? "Enable" : "Disable"), Button7	; Clear title
GuiControl, % (desc ? "Enable" : "Disable"), Button8	; Clear description
GuiControl, % (capt ? "Enable" : "Disable"), Button9	; Clear caption text
return

abort:
abort := true
SetTimer, abget, -1
return

abget:
GuiControl, Disable, Button6		; Abort
WR.Abort()		; seems OK with both call types
return

clear0:
GuiControl,, Edit1,		; Input URL
goto change
return

clear1:
GuiControl,, Edit7,		; Title
goto change
return

clear2:
GuiControl,, Edit8,		; Description
goto change
return

clear3:
GuiControl,, Edit9,		; Caption text
goto change
return

update:
Gui, Submit, NoHide
; this takes care of the stupid override in markup-enabled pages (such as WordPress)
fhost := (host = "YouTube" ? "YT" : host)
fxUrl := fxUrl ? fxUrl : inUrl	; in case we manually compose a link tag
info := ((bServer && host) ? fhost : "") . ((bOwner && fOwn) ? ((bServer && host) ? fOwn : owner) : "")
dText := (bBold ? "<b>" : "") . (info ? "[" info "] " : "") . title . (bBold ? "</b>" : "") . (bTime ? fTime : "")
;sText := (info ? "[" info "] " : "") . (desc ? desc : title) . (bTime ? fTime : "")
sText := (info ? "[" info "] " : "") . ((desc && linkType=1) ? desc : title) . (bTime ? fTime : "")
gText := sText "&#10;&#10;(click image to open video page" (host ? " on " host : "") ")"
fxUrl := fixVideoLink(fxUrl, (bSec=-1 ? oSec : bSec))
; build and select title text/image link
ids := (bSzO ? ""
	: bSzW ? " width=""" imgWSel """"
	: bSzH ? " height=""" imgHSel """"
	: bSzF ? " width=""" imgWSel """ height=""" imgHSel """"
	: "")
;ids := ids ? "`n`t`t" ids : ""
ialgn := "alignnone"
caption := "[caption id="""" align=""" ialgn """" (bSzO ? "" : " width=""" imgWSel """") "]"
iText := "<img`n`t`tclass=""" ialgn """`n`t`tsrc=""" image """`n`t`ttitle=""" gText """`n`t`talt=""" sText """`n`t`tborder=""0""" ids "/>"
ttText := (linkType=1) ? dText : iText
tgt := "_blank"
rel := "noopener"
;msgbox, linkType=%linkType%`ndText=%dText%`niText=%iText%`nttText=%ttText%
if linkType=1
	{
	r =
		(LTrim
		<a
		`thref="%fxUrl%"
		`ttitle="%desc%"
		`ttarget="%tgt%" rel="%rel%">
		`t%ttText%
		</a>
		)
	r2 =
		(LTrim
		<a
		`thref="%fxUrl%"
		`ttitle="%desc%"
		`ttarget="%tgt%" rel="%rel%">
		`t%dText%
		</a>
		)
	}
else
	{
	r =
		(LTrim
		<a
		`thref="%fxUrl%"
		`ttarget="%tgt%" rel="%rel%">
		`t%ttText%
		</a>
		)
	r2 =
		(LTrim
		<a
		`thref="%fxUrl%"
		`ttarget="%tgt%" rel="%rel%">
		`t%dText%
		</a>
		)
	r := capt ? caption r capt "[/caption]" : r
	}
GuiControl,, outCode, %r%
if InStr(fxUrl, "youtube.com")
	{
	StringReplace, Y2Murl, fxUrl, youtube, youtubepp
	GuiControl,, outY2Mate, %Y2Murl%
	}
update2:
Gui, Submit, NoHide
GuiControl, Enable, bCopy		; Copy code
if InStr(fxUrl, "youtube.com")
	GuiControl, Enable, bY2M	; Copy
testRef.innerHTML := fixOut(r2)
return

fixOut(strg)
{
StringReplace, out, strg, %A_Tab%%A_Tab%,, All
StringReplace, out, strg, %A_Tab%,, All
StringReplace, out, out, `n, %A_Space%, All
return out
}

ltsel:		; Link Type selection (Text/Image)
Gui, Submit, NoHide
GuiControl, % (linkType=1 ? "Show" : "Hide"), bBold
GuiControl, % (linkType=1 ? "Show" : "Hide"), bServer
GuiControl, % (linkType=1 ? "Show" : "Hide"), bOwner
GuiControl, % (linkType=1 ? "Show" : "Hide"), bTime
GuiControl, % (linkType=2 ? "Show" : "Hide"), bSzO
GuiControl, % (linkType=2 ? "Show" : "Hide"), bSzW
GuiControl, % (linkType=2 ? "Show" : "Hide"), bSzH
GuiControl, % (linkType=2 ? "Show" : "Hide"), bSzF
GuiControl, % (linkType=2 ? "Show" : "Hide"), imgWSel
GuiControl, % (linkType=2 ? "Show" : "Hide"), imgHSel
GuiControl, % (linkType=2 ? "Show" : "Hide"), is1
GuiControl, % (linkType=2 ? "Show" : "Hide"), is2
goto update
return

sis:		; Set image size
Gui, Submit, NoHide
if !AR := oW/oH
	return
if bSzO
	{
	GuiControl, Disable, imgWSel
	GuiControl, Disable, imgHSel
	WinMove, ahk_id %hGui2%,,,,oW, oH
	GuiControl, 2:MoveDraw, Preview, x0 y0 w%oW% h%oH%
	}
else if bSzW
	{
	GuiControl, Disable, imgHSel
	GuiControl, Enable, imgWSel
	if imgWSel < 10
		return
	h := Round(imgWSel/AR)
	WinMove, ahk_id %hGui2%,,,,imgWSel, h
	GuiControl, 2:MoveDraw, Preview, x0 y0 w%imgWSel% h%h%
	}
else if bSzH
	{
	GuiControl, Disable, imgWSel
	GuiControl, Enable, imgHSel
	if imgHSel < 10
		return
	w := Round(imgHSel*AR)
	WinMove, ahk_id %hGui2%,,,, w, imgHSel
	GuiControl, 2:MoveDraw, Preview, x0 y0 w%w% h%imgHSel%
	}
else if bSzF
	{
	GuiControl, Enable, imgWSel
	GuiControl, Enable, imgHSel
	if (imgWSel < 10 OR imgHSel < 10)
		return
	WinMove, ahk_id %hGui2%,,,,imgWSel, imgHSel
	GuiControl, 2:MoveDraw, Preview, x0 y0 w%imgWSel% h%imgHSel%
	}
goto update
return

copy:
Clipboard := fixOut(r)
Tooltip, Code copied to clipboard
SetTimer, tt, -3500
return

copyY2M:
Gui, Submit, NoHide
Clipboard := outY2Mate
Tooltip, Y2Mate code copied to clipboard
SetTimer, tt, -3500
return

tt:
Tooltip
return

showhide:
sh := !sh
Gui, Show, % (sh ? "" : "Hide")
return

aot:
aot := !aot
Gui, % (aot ? "+" : "-") "AlwaysOnTop"
Menu, Tray, % (aot ? "Check" : "Uncheck"), AlwaysOnTop
return

about:
Gui +OwnDialogs
MsgBox 0x40080, About %appname%,
(
%appname% v%version% %releaseT%
released %releaseD%
by %author%

Takes a simple URL and formats it as HTML,
optionally retrieving data from a server
(such as Vimeo, YouTube, IMDb, Cinemagia).
Can format data as a Link or clickable picture.

This application is completely free open-source,
developed in AutoHotkey %A_AhkVersion% %releaseE%.

Latest sources and compiled version are found at:
https://gitlab.com/windows-wine/fixvideolink
You may freely copy/modify/distribute it
according to the GPLv2 (or later) license. Enjoy!

Other work can be found in large package at:
https://www.dropbox.com/s/fi2iuxaqy3o713n/public_work.7z?dl=1
while possibly updated versions may be found at:
https://gitlab.com/windows-wine

© %author% %releaseD%
)
DllCall("DestroyIcon", "Ptr", hIcon)
return

reload:
Reload
Sleep, 300
;================================================================
;		FUNCTIONS
;================================================================
OnMsgBox()
;================================================================
{
Global
DetectHiddenWindows, On
Process, Exist
If (WinExist("ahk_class #32770 ahk_pid " . ErrorLevel))
	{
	hIcon := LoadPicture(A_IsCompiled ? A_ScriptFullPath : iconlocal, "w48 Icon1", _)
	SendMessage 0x172, 1, %hIcon% , Static1
	}
}
;================================================================
fixVideoLink(url, lt=-1)	; lt=link type: 0=unsecured, -1=as-is, 1=secured
;================================================================
{
Global
; needs addition for FB video url/player AND fix for well-formated YouTube/Vimeo/etc links
vUrl := res := ""
if url contains ://youtu.be,://www.youtube.com
	RegExMatch(url, "(?<=[\?&]v=|embed/|//youtu\.be/).*?((?=[\s&\?])|$)", res)
else if url contains vimeo.com
	vUrl := RegExReplace(url, "player\.vimeo\.com/video", "vimeo.com")
oUrl := res ? "http://www.youtube.com/watch?v=" res : vUrl ? vUrl : url
return RegExReplace(oUrl, "^https?", (lt=1 ? "https" : lt=0 ? "http" : lt=-1 ? oSec : "https")) ; default https
}
;================================================================
;https://www.autohotkey.com/boards/viewtopic.php?p=404987#p404987
; expiry date as: Fri,01-Jan-2038 00:00:00 GMT
setCookie(domain, value, exp="")	; thanks malcev
;================================================================
{
cv := !exp ? value : value "; expires = " exp
return DllCall("wininet\InternetSetCookie" AW
			, "Str", domain
			, "Ptr", 0
			, "Str", cv)
}
;================================================================
getData(WHO, url)
;================================================================
{
Global
host := owner := time := title := desc := data := dims := upDate := outCode := fOwn := fTime := ""
by := at := image := type := ""
if !url
	return
;=== cURL commands ===
curl_easy_setopt(hnd, "CURLOPT_URL", url)
result := {bin:"", size: 0}
curl_easy_setopt(hnd, "CURLOPT_WRITEDATA", &result)
curl_easy_perform(hnd)
RT := StrGet(result.addr, result.size, "utf-8")
;=== /cURL commands ===
if abort
	return url
if (e := A_LastError)
	{
	SetFormat, Integer, H
	e+=0x00000000FFFFFFFF+1
	FileAppend, Error %e% for %url%`n, Link errors.txt
	GuiControl,, host,
	GuiControl,, owner,
	GuiControl,, time,
	GuiControl,, dims,
	GuiControl,, added,
	GuiControl,, title, %url%
	GuiControl,, outCode,
	GuiControl,, desc, % FormatMessage("getData()", e)
	SetFormat, Integer, D
	return url
	}

if debug
	FileAppend, % "<!-- " url " -->`n`n" RT, currentPage.txt, UTF-8
; generic tags read here, whatever doesn't yield we get further below
; <meta property="og:image" content="https://scontent.fotp3-3.fna.fbcdn.net/v/t15.0-10/24541542_10210885701529080_2440364378516619264_n.jpg?oh=f01a4981ba7c15c7746f46491d942f27&amp;oe=5AFD3543" /> won't work (Bad URL date param) FB are imbecils!
RegExMatch(RT, "(?<=<meta property=(""|')og:title(""|') content=(""|')).*?(?=(""|')\s*/?>)", title)
RegExMatch(RT, "(?<=<meta property=(""|')og:site_name(""|') content=(""|')).*?(?=(""|')\s*/?>)", host)
RegExMatch(RT, "(?<=<meta property=(""|')og:description(""|') content=(""|')).*?(?=(""|')\s*/?>)", desc)
RegExMatch(RT, "(?<=<meta property=(""|')og:image(""|') content=(""|')).*?(?=(\?|&|(""|')\s*/?>))", image)
RegExMatch(RT, "(?<=<meta property=(""|')og:image:width(""|') content=(""|')).*?(?=(""|')\s*/?>)", iW)
RegExMatch(RT, "(?<=<meta property=(""|')og:image:height(""|') content=(""|')).*?(?=(""|')\s*/?>)", iH)
RegExMatch(RT, "(?<=<meta property=(""|')og:video:width(""|') content=(""|')).*?(?=(""|')\s*/?>)", vidW)
RegExMatch(RT, "(?<=<meta property=(""|')og:video:height(""|') content=(""|')).*?(?=(""|')\s*/?>)", vidH)
RegExMatch(RT, "(?<=<meta property=(""|')og:type(""|') content=(""|')).*?(?=(""|')\s*/?>)", type)
;
RegExMatch(RT, "m)(?<=<title>).*?(?=</title>)", pTitle)	; page title can tell us something
RegExMatch(RT, "(?<=<meta name=(""|')description(""|') content=(""|')).*?(?=(""|')\s*/?>)", desc2)
if InStr(pTitle, " - YouTube")
	{
	desc=%desc%
	desc := desc ? desc : desc2 ? desc2 : title
	RegExMatch(RT, "(?<=<meta itemprop=""duration"" content="").*?(?="">)", rawtime)
	if rawtime
		{
		StringReplace, time, rawtime, PT,,
		StringReplace, time, time, S,,
		StringReplace, time, time, M, :,
		StringSplit, t, time, :
		th := SubStr("00" t1//60, -1)
		tm := SubStr("00" t1-60*th, -1)
		ts := SubStr("00" t2, -1)
		time := th ":" tm ":" ts
		}
	RegExMatch(RT, "(?<=<meta itemprop=""datePublished"" content="").*?(?=""\s*/?>)", upDate)
/*
	RegExMatch(RT, "(?<=ytplayer.config = \{).*?(?=};)", data)
	StringReplace, data, data, ",, All
	StringReplace, data, data, }`,, |, All
	Loop, Parse, data, |
		if InStr(A_LoopField, "author:")
			{
			RegExMatch(A_LoopField, "(?<=author:).*?(?=,)", owner)
			break
			}
*/
; <link itemprop="name" content="Juan Classic 2"></span>
; "author":"Juan Classic 2", "ownerChannelName":"Juan Classic 2"
	if !RegExMatch(RT, "(?<=<link itemprop=""name"" content="").*?(?="">)", owner)
		if !RegExMatch(RT, "(?<=""author"":"").*?(?="")", owner)
			if !RegExMatch(RT, "(?<=""ownerChannelName"":"").*?(?="")", owner)
				msgbox, this is fucked up! author can't be retrieved!
	}
else if (pTitle = "YouTube")	; video missing/deleted/unavailable due to racism
	{
	desc := "missing/deleted/unavailable video link"
	title := "[dead video link]"
	RegExMatch(RT, "(?<=<meta itemprop=""regionsAllowed"" content="").*?(?=""\s*/?>)", reg)
	if reg && !InStr(reg, ",RO")
		msgbox, Racists don't allow this clip to be viewed in your country!!!
	}
else if InStr(pTitle, " on Vimeo")
	{
	RegExMatch(RT, "(?<=window.vimeo.clip_page_config = \{).*?(?=};)", data)
	StringReplace, data, data, ",, All
	StringReplace, data, data, }`,, |, All
/*
get window.vimeo.clip_page_config = {�}; where:
"uploaded_on":"<data>"
"duration":{"raw":xx,"formatted":"HH:MM:SS"}
"owner":{"id":xxxxx,"display_name":"<data>"
"dimensions":{"height":xxx,"width":yyy}
"thumbnail":{"src":"<url>","src_2x": "url2>",etc}

or for owner/author try:
{"@id":"https://vimeo.com/userXXXXXXXXX","name":"<username>"}
*/
	Loop, Parse, data, |
		{
		if InStr(A_LoopField, "uploaded_on:")
			RegExMatch(A_LoopField, "(?<=uploaded_on:).*?(?=,)", upDate)
		else if InStr(A_LoopField, "duration:")
			RegExMatch(A_LoopField, "(?<=formatted:).*", time)
		else if InStr(A_LoopField, "owner:")
			RegExMatch(A_LoopField, "(?<=display_name:).*?(?=,)", owner)
		else if InStr(A_LoopField, "dimensions:")
			{
			RegExMatch(A_LoopField, "(?<=dimensions:\{).*", size)
			Loop, Parse, size, CSV
				if InStr(A_LoopField, "height:")
					vidH := SubStr(A_LoopField, 8)
				else if InStr(A_LoopField, "width:")
					vidW := SubStr(A_LoopField, 7)
			}
		}
	desc := desc ? desc : "video link"
	}
else if InStr(pTitle, " - Rotten Tomatoes")
	{
; <img src="https://resizing.flixster.com/F7TcYkOm9FMYJdzFfDUCUlvXOWg=/206x305
; /v1.bTsxMjUxNDk5MztwOzE3NTU5OzEyMDA7NDY1OzY4OA" alt="Coco" class="posterImage">
	RegExMatch(RT, "(?<=<img src="").*?(?=""\s?alt="".*?""\s?class=""posterImage"">)", image)
	}
else
	{
	title := title ? title : pTitle ? pTitle : "Unknown"
	if !host	; get host from domain name if page doesn't reveal such property
		{
		host := GetDomain(url)
		StringLower, host, host, T
		}
; og:description can be very lengthy (for example on cinemagia.ro) so we won't use it for now
	desc := desc2 ? desc2 : desc ? desc : "link"
	}
if !title
	return url
if image
	{
	if host="IMDb")
		image := RegExReplace(image, "_CR.*_", "_")	; IMDb has a nasty habit of cropping posters
	}

; new video site: "https://videopress.com/embed/axSlYsYa?hd=0&autoPlay=0&permalink=0&loop=0"
dims := (vidW && vidH) ? vidW "x" vidH : "unknown"
GuiControl,, host, % host
GuiControl,, owner, % owner
GuiControl,, time, % time
GuiControl,, dims, % dims
GuiControl,, added, % upDate
GuiControl,, title, % title
GuiControl,, desc, % desc
;GuiControl,, altern, % type
GuiControl,, capt,
fTime := time ? " (" time ")" : ""
fOwn := owner ? " -&gt; " owner : ""
by := owner ? " by " owner : ""
at := (host != "???") ? " at " host : ""
dataReady := true
}
;================================================================
getSC()
;================================================================
{
SetFormat, Integer, H
c := SubStr(DllCall("GetSysColor", "Int", 15), 3)	; COLOR_3DFACE
SetFormat, Integer, D
return % "#" SubStr("000000" c, -5)
}
;================================================================
setABC(hwnd, msg, wP, lP)
;================================================================
{
Global
test.document.body.style.backgroundColor := getSC()
}
;================================================================
GetDomain(inUrl)
;================================================================
{
Static padd := A_Is64bitOS ? 4 : 0	; struct padding unit (DWORD)
Static sz := 9*4+6*A_PtrSize+5*padd	; 60 in x86, 80 in x64
VarSetCapacity(UrlComp, sz, 0)		; URL_COMPONENTS struct
NumPut(sz, UrlComp, 0, "UInt")		; dwStructSize
VarSetCapacity(host, 512, 0)		; buffer for HostName, 256 Unicode chars (max domain is 253)
NumPut(256, UrlComp, 3*4+2*A_PtrSize+2*padd, "UInt")	; dwHostNameLength
NumPut(&host, UrlComp, 3*4+A_PtrSize+padd, "Ptr")		; lpszHostName
if !DllCall("wininet\InternetCrackUrl" AW
	, "Str"	, inUrl			; lpszUrl
	, "UInt"	, 0					; dwUrlLength
	, "UInt"	, 0					; dwFlags
	, "Ptr"	, &UrlComp)	; lpUrlComponents
	msg := FormatMessage(" InternetCrackUrl()", A_LastError)
else
	{
	VarSetCapacity(host, -1)
	StringSplit, s, host, .
	sa := s0-1, sb := s0-2
	if p := InStr(s%s0%, ":")	; has port number
		StringLeft, s%s0%, s%s0%, p-1
	if s0=1					; some long integer as domain, dodgy usage; or localhost
		domain := s1
	else if s0=4
		{
		if s1 is digit
		if s2 is digit
		if s3 is digit
		if s4 is digit
		domain := s1 "." s2 "." s3 "." s4	; it's an IPv4
		}
	else if StrLen(s%sa%) < 4	; assume subdomain names may be 3 chars or less
		domain := s%sb%
	else domain := s%sa%
	msg := domain
	}
return msg
}
;================================================================
FormatMessage(ctx, msg, arg="")
;================================================================
{
Global
Local txt, buf
SetFormat, Integer, H
msg+=0
SetFormat, Integer, D
DllCall("FormatMessage" AW
	, "UInt",	0x1100	; FORMAT_MESSAGE_FROM_SYSTEM/ALLOCATE_BUFFER
	, "UInt",	0		; lpSource
	, "UInt",	msg		; dwMessageId
	, "UInt",	0		; dwLanguageId (0x0418=RO)
	, "PtrP",	buf		; lpBuffer
	, "UInt",	0		; nSize
	, "Str",	arg)	; Arguments
txt := DllCall("MulDiv", "Ptr", buf, "Int", 1, "Int", 1, "Str")
DllCall("LocalFree", "Ptr", buf)
return "Error " msg " in " ctx ":`n" txt
}
;================================================================
Gdip_On(ByRef hGdip)
;================================================================
{
Global AW
if hGdip := DllCall("LoadLibrary" AW, "Str", "gdiplus.dll")
	{
	VarSetCapacity(si, 16, 0), si := Chr(1)
	DllCall("gdiplus\GdiplusStartup", "PtrP", tok, "Ptr", &si, "Ptr", 0)
	if !tok
		{
		msgbox, can't initialize gdiplus
		return
		}
	}
else
	{
	msgbox, can't load gdiplus
	return
	}
return tok
}
;================================================================
Gdip_Off(hLib, tok)
;================================================================
{
DllCall("gdiplus\GdiplusShutdown", "Ptr", tok)
DllCall("FreeLibrary", "Ptr", hLib)
}
;================================================================
GuidFromString(guid, ByRef buf)
;================================================================
{
Static pr
if !pr
	if h := DllCall("GetModuleHandle", "Str", "shell32.dll")	; shlwapi.dll -> 269 (A) 270 (W)
		pr := DllCall("GetProcAddress", "Ptr", h, "UInt", (A_IsUnicode ? 704 : 703), "Ptr")
VarSetCapacity(buf, 16, 0)
return DllCall(pr, "Str", guid, "Ptr", &buf)
}
;================================================================
Gdip_FitImageEx(fPath, hCtrl, blur=0)
;================================================================
{
Global abort

if InStr(fPath, "http")=1
	{
	SplitPath, fPath,,, fExt
	outfile := "tmpImgFile." fExt
	if debug
		{
		FileAppend, %A_ThisFunc%(): useXML=%useXML%`tfPath=%fPath%`toutfile=%outfile%`n, debug.txt
		if !FileExist(outfile)
			UrlDownloadToFile, %fPath%, %outfile%
		if e := ErrorLevel
			FileAppend, %A_ThisFunc%() error %A_LastError%, debug.txt
		}
	if useXML
		WHI := ComObjCreate("Msxml2.XMLHTTP")
	else
		WHI := ComObjCreate("WinHttp.WinHttpRequest.5.1")
	WHI.Open("GET", fPath, true)	; true=async, false=sync
	WHI.SetRequestHeader("Pragma", "no-cache")
	WHI.SetRequestHeader("Cache-Control", "no-cache, no-store")
	WHI.SetRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT")
	WHI.SetRequestHeader("Content-Type", "application/binary")
;	WHI.SetRequestHeader("Referer", fxUrl)

	if InStr(fPath, "youtube")
		{
		if useXML
			setCookie("https://www.youtube.com", "CONSENT=YES+EN")
		else
			WHI.SetRequestHeader("Cookie", "CONSENT=YES+EN")	; not perfect
		}

	if useXML
		{
		WHI.onreadystatechange := Func("ImgDown").Bind(WHI, hCtrl, blur)
		WHI.Send()
		}
	else
		{
		WHI.Option(6) := true		; WinHttpRequestOption_EnableRedirects
		WHI.SetTimeouts(0, 60000, 30000, 180000)	; last timeout is ReceiveTimeout
		WHI.Send(), WHI.WaitForResponse(180)	; only in async mode
		return ImgStream(WHI, hCtrl, blur)
		}
	}
else
	{
	if !A_IsUnicode
		{
		sz := 2*(StrLen(fPath)+1)
		VarSetCapacity(fName, sz, 0), StrPut(fPath, &fName, sz, "cp1200")
		}
	else fName := fPath
	DllCall("gdiplus\GdipCreateBitmapFromFile", "Ptr", &fName, "PtrP", pImg)
	return PaintImg(pImg, hCtrl, blur)
	}
}
;================================================================
ImgDown(reqObj, hCtrl=0, blur=0)
;================================================================
{
Global abort
if abort
	reqObj.Abort()
if reqObj.readyState != 4
	return
return ImgStream(reqObj, hCtrl, blur)
}
;================================================================
ImgStream(reqObj, hCtrl=0, blur=0)
;================================================================
{
Static IID_IStream := "{0000000C-0000-0000-C000-000000000046}"
if RS := ComObjQuery(reqObj.ResponseStream, IID_IStream)
	{
	if r := DllCall("gdiplus\GdipCreateBitmapFromStream", "Ptr", RS, "PtrP", pImg)
		FileAppend, %A_ThisFunc%(): Error`, GdipCreateBitmapFromStream() returned %r%`n, debug.txt
	}
else FileAppend, %A_ThisFunc%(): Error`, ResponseStream is null`n, debug.txt
ObjRelease(reqObj)
if debug
	FileAppend, %A_ThisFunc%(): pImg=%pImg%`thCtrl=%hCtrl%`tblur=%blur%`tRS=%RS%`n, debug.txt
return PaintImg(pImg, hCtrl, blur)
}
;================================================================
PaintImg(pImg, hCtrl, blur=0)
;================================================================
{
Global hBmp2, hGui2, hPreview, oW, oH, hDown, hNoimg
Static BlurGuid := "{633C80A4-1843-482b-9EF2-BE2834C5FDD4}"
if !pImg
	{
	if debug
		FileAppend, %A_ThisFunc%(): pImg=%pImg%`thCtrl=%hCtrl%`tblur=%blur%`n, debug.txt
	return
	}
VarSetCapacity(rect, 16, 0)
DllCall("GetClientRect", "Ptr", hCtrl, "Ptr", &rect)
cw := NumGet(rect, 8, "Int")-NumGet(rect, 0, "Int")
ch := NumGet(rect, 12, "Int")-NumGet(rect, 4, "Int")
DllCall( "gdiplus\GdipGetImageWidth" , "Ptr", pImg, "UIntP", oW)
DllCall( "gdiplus\GdipGetImageHeight", "Ptr", pImg, "UIntP", oH)
nBmp := ScaleRect(oW, oH, cw, ch)
DllCall("gdiplus\GdipCreateBitmapFromScan0"
	, "UInt", nBmp.W
	, "UInt", nBmp.H
	, "Int", 0
	, "UInt", 0x26200A	; PixelFormat32bppARGB
;	, "UInt", 0x021808	; PixelFormat24bppRGB
;	, "UInt", 0x0E200B	; PixelFormat32bppPARGB
	, "Ptr", 0
	, "PtrP", pB)
DllCall("gdiplus\GdipGetImageGraphicsContext", "Ptr", pB, "PtrP", pG)
if !pG
	return	; what about pB, don't we delete that bitmap?
; DllCall("gdiplus\GdipSetSmoothingMode", "Ptr", pG, "Int", 2)	; High=2, AA=4
DllCall("gdiplus\GdipSetInterpolationMode", "Ptr", pG, "UInt", 7)	; use 2 or 7
DllCall("gdiplus\GdipDrawImageRectI"
	, "Ptr", pG
	, "Ptr", pImg
	, "UInt", NumGet(rect, 0, "UInt")
	, "UInt", NumGet(rect, 4, "UInt")
	, "UInt", nBmp.W ;NumGet(rect, 8, "UInt")
	, "UInt", nBmp.H ;NumGet(rect, 12, "UInt")
	, "UInt")
if blur
	{
	VarSetCapacity(bp, 8, 0), NumPut(blur, bp, 0, "Float") ; radius
	GuidFromString(BlurGuid, buf)
	DllCall("gdiplus\GdipCreateEffect"
		, "UInt", NumGet(buf, 0, "UInt")
		, "UInt", NumGet(buf, 4, "UInt")
		, "UInt", NumGet(buf, 8, "UInt")
		, "UInt", NumGet(buf, 12, "UInt")
		, "PtrP", pEffect)
	DllCall("gdiplus\GdipSetEffectParameters"
		, "Ptr", pEffect
		, "Ptr", &bp
		, "UInt", 8)
	DllCall("gdiplus\GdipBitmapApplyEffect"
		, "Ptr", pB
		, "Ptr", pEffect
		, "Ptr", &rect
		, "Ptr", 0
		, "Ptr", 0
		, "Ptr", 0)
	DllCall("gdiplus\GdipDeleteEffect", "Ptr", pEffect)
	}
DllCall("gdiplus\GdipCreateHBITMAPFromBitmap"
	, "Ptr", pB
	, "PtrP", hBmp
	, "Int", 0xFFFFFFFF)
SendMessage, 0x172, 0, 0,, % "ahk_id " hCtrl
if ErrorLevel not in %hDown%,%hNoimg%
	DllCall("DeleteObject", "Ptr", ErrorLevel)
SendMessage, 0x172, 0, hBmp,, % "ahk_id " hCtrl
DllCall("gdiplus\GdipCreateHBITMAPFromBitmap"
	, "Ptr", pImg
	, "PtrP", hBmp2
	, "Int", 0xFFFFFFFF)
WinMove, ahk_id %hGui2%,,,,oW, oH
WinMove, ahk_id %hPreview%,, 0, 0,oW, oH
SendMessage, 0x172, 0, 0,, % "ahk_id " hPreview
DllCall("DeleteObject", "Ptr", ErrorLevel)
SendMessage, 0x172, 0, hBmp2,, % "ahk_id " hPreview
DllCall("gdiplus\GdipDeleteGraphics", "Ptr", pG)
DllCall("gdiplus\GdipDisposeImage", "Ptr", pImg)
dataReady := true
return hBmp
}
;================================================================
; Function by SKAN | Created: 19-July-2017
ScaleRect( SW, SH, TW, TH, Upscale := 0 ) {
;================================================================
Local  SAF := SW/SH, TAF := TW/TH ; Aspect ratios of Source and Target
Return  ( !Upscale and SW <= TW and SH <= TH ) ? {W: SW, H: SH}
	:   ( SAF < TAF ) ? { W: Floor( ( TW / TAF ) * SAF ), H: TH}
	:   ( SAF > TAF ) ? { W: TW, H: Floor( ( TH / SAF ) * TAF )}
	:   { W: TW, H: TH }
}
;================================================================
LoadImage(src, idx, t)
;================================================================
{
Static it="BIC"
t := InStr(it, t)-1
if t<0
	return 0
Loop, %src%
	if A_LoopFileExt in exe,dll
		{
		if !hInst := DllCall("GetModuleHandle" AW, "Str", src, "Ptr")
			hL := hInst := DllCall("LoadLibraryEx" AW
						, "Str", src
						, "UInt", 0
						, "UInt", 0x2
						, "Ptr")
				; LOAD_LIBRARY_AS_DATAFILE.
		if idx is not integer
			type := "Str"
		else type := "UInt"
		hIcon := DllCall("LoadImage" AW
				, "Ptr", hInst
				, type, idx
				, "UInt", t
				, "Int", 0
				, "Int", 0
				, "UInt", 0x2000
				, "Ptr")
		if (hInst && hL)
			DllCall("FreeLibrary", "Ptr", hInst)
		DllCall("DeleteObject"," Ptr", hInst)
		}
	else if A_LoopFileExt in bmp,ico,cur,ani
		{
		hIcon := DllCall("LoadImage" AW
				, "Ptr", 0
				, "Str", src
				, "UInt", t
				, "Int", 0
				, "Int", 0
				, "UInt", 0x2010
				, "Ptr")
		}
	else
		{
		hIcon := DllCall("LoadImage" AW
				, "Ptr", -1
				, "UInt", idx
				, "UInt", t
				, "Int", 0
				, "Int", 0
				, "UInt", 0x8000
				, "Ptr")
		}
return hIcon
}
;================================================================
write_callback(pBuffer, size, nmemb, userdata)
;================================================================
{
	realsize := size * nmemb
	dl := Object(userdata)
	dl.SetCapacity("bin", dl.size + realsize + 1)
	dl.addr := dl.GetAddress("bin")
	DllCall("RtlMoveMemory"
		, "Ptr", dl.addr+dl.size
		, "Ptr", pBuffer
		, "UInt", realsize)
	dl.size += realsize
	return realsize
}
